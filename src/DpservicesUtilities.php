<?php

namespace Drupal\dpservices;

/**
 * Defines Dpservices Properties. 
 */
final class DpservicesUtilities {
  
  /**
   * Request Url.
   *
   * @var string
   */

  const DP_REQUEST_URL = '/dpservices/deploy/entity.json';  

   /**
   * Request Method.
   *
   * @var string
   */

  const DP_REQUEST_METHOD = 'POST'; 

  /**
   * Checkbox field for deploy status for Node.
   *
   * @var string
   */

  const DP_FIELD_NODE_DEPLOY = 'field_node_deploy'; 

  /**
   * Checkbox field for deploy status for Block.
   *
   * @var string
   */

  const DP_FIELD_BLOCK_DEPLOY = 'field_block_deploy';

  /**
   * File field type for generate Entity Array.
   *
   * @var string
   */

  const DP_FIELD_FILE_TYPE = ['image' , 'video' , 'file'];

  /**
   * Common field type for generate Entity Array.
   *
   * @var Array
   */
  const DP_FIELD_COMMON_INDEX =  ['image' , 'video' , 'file' , 'entity_reference']; 

}
