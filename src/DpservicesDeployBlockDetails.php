<?php

namespace Drupal\dpservices;

use Drupal\node\Entity\Node;
use Drupal\dpservices\DpservicesSchemaDetails;
use Drupal\dpservices\DpservicesGenerateEntityArray;
use Drupal\dpservices\FieldDetails;
use Drupal\dpservices\DpservicesUtilities;
use Drupal\block_content\Entity\BlockContent;
use Drupal\dpservices\EntityTranslationDetails;


/**
 * Defines Block Details functionalities. 
 */
class DpservicesDeployBlockDetails {  

  /**
   * Get Node deploy details.
   */
  public static function getDeployBlockDetails() {
    $ids = DpservicesSchemaDetails::getBlockDeployDetails();
    $block = [];
    foreach($ids as $index => $id) {
      $bid = $id['bid'];    
      //$node_obj = Node::load($nid);
      $block_obj = BlockContent::load($bid);
      $block_obj = EntityTranslationDetails::getEntityTranslation($block_obj, $id['langcode']);      
      $block[$index]['bid']['value'] = $block_obj->get('id')->getvalue();
      $block[$index]['type']['value'] = $block_obj->get('type')->getvalue();
      $block[$index]['langcode']['value'] = $id['langcode'];
      $ctype = $block_obj->get('type')->getvalue()[0]['target_id'];	
      $bundle = 'block_content';
      $bundleFields = FieldDetails::getFieldList($bundle, $ctype);	  
      foreach($bundleFields['block_content'] as $key => $field) {        
        $value = $block_obj->get($key)->getvalue();	
        //print_r($field_type);	
        $field_type = $block_obj->get($key)->getFieldDefinition()->getType();
        $datatype = $field['datatype'];
        if(in_array($field_type , DpservicesUtilities::DP_FIELD_FILE_TYPE)) {
          $block = DpservicesGenerateEntityArray::getEntityArrayByFileType($value , $key , $index, $block, $datatype);				 
        }
        else if($field_type == 'entity_reference_revisions') {
          $values = $block_obj->get($key)->getvalue();			
          if($field['datatype'] == 'paragraph') {	
            $paragraphs = $block_obj->get($key)->referencedEntities();
            $arr_paragraph = [];		
            $arr_paragraph = DpservicesGenerateEntityArray::getEntityArrayByParagraph($paragraphs, $key, $block,  $arr_paragraph, $id['langcode']);
            $block[$index]['entity'][$key]['value'] = $arr_paragraph;
            $block[$index]['entity'][$key]['type'] = $datatype;
          }			    		
        }
        else if($field_type == 'path') {
          $block = DpservicesGenerateEntityArray::getEntityArrayByPath($value , $key , $index, $block, $datatype);   		
        }
        else {
          if($key != DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY) {
            if($key == 'changed') {
              $block[$index]['entity'][$key]['value'][0]['value'] = time();
            }
            else {
              $block[$index]['entity'][$key]['value'] = $block_obj->get($key)->getvalue();
            }
          }          
        }
      }
    }
    $arr_block = [];
    if(isset($block)) {
      $arr_block = $block;
    }
    return $arr_block;
  } 
  
}
