<?php

namespace Drupal\dpservices;

/**
 * Defines field details. 
 */
class FieldDetails {

  /**
   * Defines method to get list of fields from Entity type. 
   */
  public static function getFieldList($bundle, $cType) {    
    $field_defs = \Drupal::service('entity_field.manager')->getFieldDefinitions($bundle, $cType);    
    foreach ($field_defs as $field_name => $field_definition) {      
      if (!empty($field_definition->getTargetBundle())) {
        $datatype = '';
        if(isset($field_definition->getSettings()['handler'])) {
          $handler = $field_definition->getSettings()['handler'];
          $arr_handler = explode(':', $handler); 
          $datatype = isset($arr_handler[1])? $arr_handler[1] : '';
        }
        $bundleFields[$bundle][$field_name]['type'] = $field_definition->getType();
        $bundleFields[$bundle][$field_name]['label'] = $field_definition->getLabel();
        $bundleFields[$bundle][$field_name]['datatype'] = $datatype;
      }
    } 
    return $bundleFields;
  }
  
}
