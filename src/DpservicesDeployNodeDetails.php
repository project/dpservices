<?php

namespace Drupal\dpservices;

use Drupal\node\Entity\Node;
use Drupal\dpservices\DpservicesSchemaDetails;
use Drupal\dpservices\DpservicesGenerateEntityArray;
use Drupal\dpservices\FieldDetails;
use Drupal\dpservices\DpservicesUtilities;
use Drupal\dpservices\EntityTranslationDetails;

/**
 * Defines Node Details functionalities. 
 */
class DpservicesDeployNodeDetails {  

  /**
   * Get Node deploy details.
   */
  public static function getDeployNodeDetails() {
    $ids = DpservicesSchemaDetails::getNodeDeployDetails();
    $node = [];
    foreach($ids as $index => $id) {
      $nid = $id['nid'];    
      $node_obj = Node::load($nid);
      $node_obj = EntityTranslationDetails::getEntityTranslation($node_obj, $id['langcode']);      
      $node[$index]['nid']['value'] = $node_obj->get('nid')->getvalue();
      $node[$index]['type']['value'] = $node_obj->get('type')->getvalue();
      $node[$index]['langcode']['value'] = $id['langcode'];
      $ctype = $node_obj->get('type')->getvalue()[0]['target_id'];	
      $bundle = 'node';
      $bundleFields = FieldDetails::getFieldList($bundle, $ctype);	  
      foreach($bundleFields['node'] as $key => $field) {        
        $value = $node_obj->get($key)->getvalue();	
        //print_r($field_type);	
        $field_type = $node_obj->get($key)->getFieldDefinition()->getType();
        $datatype = $field['datatype'];
        if(in_array($field_type , DpservicesUtilities::DP_FIELD_FILE_TYPE)) {
          $node = DpservicesGenerateEntityArray::getEntityArrayByFileType($value , $key , $index, $node, $datatype);				 
        }
        else if($field_type == 'entity_reference_revisions') {
          $values = $node_obj->get($key)->getvalue();			
          if($field['datatype'] == 'paragraph') {	
            $paragraphs = $node_obj->get($key)->referencedEntities();
            $arr_paragraph = [];		
            $arr_paragraph = DpservicesGenerateEntityArray::getEntityArrayByParagraph($paragraphs, $key, $node,  $arr_paragraph, $id['langcode']);
            $node[$index]['entity'][$key]['value'] = $arr_paragraph;
            $node[$index]['entity'][$key]['type'] = $datatype;
          }			    		
        }
        else if($field_type == 'path') {
          $node = DpservicesGenerateEntityArray::getEntityArrayByPath($value , $key , $index, $node, $datatype);   		
        }
        else {
          if($key != DpservicesUtilities::DP_FIELD_NODE_DEPLOY) {
            if($key == 'changed') {
              $node[$index]['entity'][$key]['value'][0]['value'] = time();
            }
            else {
              $node[$index]['entity'][$key]['value'] = $node_obj->get($key)->getvalue();
            }
          }          
        }
      }
    }
    $arr_node = [];
    if(isset($node)) {
      $arr_node = $node;
    }
    return $arr_node;
  } 
  
}
