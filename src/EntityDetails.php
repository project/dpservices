<?php

namespace Drupal\dpservices;

/**
 * Defines class to get entity details. 
 */
class EntityDetails {

  /**
   * Defines method to check Node Exists.
   */
  public static function checkNodeExists($nid) {
    $query = \Drupal::entityQuery('node');
    $query->condition('nid', $nid);   
    $id = $query->execute();
    return $id;
  }

  /**
   * Defines method to check Block Exists.
   */
  public static function checkBlockExists($bid) {
    $query = \Drupal::entityQuery('block_content');
    $query->condition('id', $bid);   
    $id = $query->execute();
    return $id;
  }
  
}
