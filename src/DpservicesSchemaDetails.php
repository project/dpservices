<?php

namespace Drupal\dpservices;

use Drupal\dpservices\DpservicesUtilities;
use Drupal\node\Entity\Node; 
use Drupal\block_content\Entity\BlockContent;
use Drupal\dpservices\EntityTranslationDetails;

/**
 * Defines Schema functionalities. 
 */
class DpservicesSchemaDetails {  

  /**
   * Get Node deploy details.
   */
  public static function getNodeDeployDetails() {
    $query = \Drupal::database()->select('dpservices' , 'n');
    $query->fields('n' , array('entity_id' , 'deploy_date', 'langcode'));
    $query->orderBy('default_langcode' , 'DESC');	
    $query->condition('status', 0, "=");
    $query->condition('type', 'node', "=");
    $results = $query->execute()->fetchAll();
    $ids = array();
    $time = getdate();
    $time = $time[0];
    foreach ($results as $key => $value) {
      $deploy_date = $value->deploy_date;	 
      if($time >= $deploy_date) {
        $ids[$key]['nid'] = $value->entity_id;
        $ids[$key]['langcode'] = $value->langcode;
      }
    } 
    return $ids;
  } 

  /**
   * Update Entity deploy details.
   */
  public static function UpdateEntityDeployDetails($results) {
    if(isset($results['data']['node'])) {
      foreach($results['data']['node'] as $key => $value) {
        $node_obj = Node::load($value['id']);
        $node_obj = EntityTranslationDetails::getEntityTranslation($node_obj, $value['langcode']); 
	      $node_obj->set(DpservicesUtilities::DP_FIELD_NODE_DEPLOY , 0);
	      $node_obj->save();
		    $query = \Drupal::database()->update('dpservices');
        $query->fields(['status' => 1 ]);
        $query->condition('entity_id', $value['id']);
        $query->condition('langcode', $value['langcode']);
		    $query->execute();
      }
	  }
    if(isset($results['data']['block'])) {
      foreach($results['data']['block'] as $key => $value) {
        $block_obj = BlockContent::load($value['id']);
        $block_obj = EntityTranslationDetails::getEntityTranslation($block_obj, $value['langcode']); 
	      $block_obj->set(DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY , 0);
	      $block_obj->save();
		    $query = \Drupal::database()->update('dpservices');
        $query->fields(['status' => 1 ]);
        $query->condition('entity_id', $value['id']);
        $query->condition('langcode', $value['langcode']);
		    $query->execute();
      }
	  }
  }

  /**
   * Get Block deploy details.
   */
  public static function getBlockDeployDetails() {
    $query = \Drupal::database()->select('dpservices' , 'n');
    $query->fields('n' , array('entity_id' , 'deploy_date', 'langcode'));
    $query->orderBy('default_langcode' , 'DESC');	
    $query->condition('status', 0, "=");
    $query->condition('type', 'block_content', "=");
    $results = $query->execute()->fetchAll();
    $ids = array();
    $time = getdate();
    $time = $time[0];
    foreach ($results as $key => $value) {
      $deploy_date = $value->deploy_date;	 
      if($time >= $deploy_date) {
        $ids[$key]['bid'] = $value->entity_id;
        $ids[$key]['langcode'] = $value->langcode;
      }
    } 
    return $ids;
  }
  
}
