<?php

namespace Drupal\dpservices;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\dpservices\FieldDetails;

/**
 * Generate Storage Entity Array.
 */
class DpservicesGenerateStorageEntityArray {

  /**
   * Generate Storage Entity Array for File Type.
   */
  public static function getEntityArrayByFile($values) {    
    $config = \Drupal::config('dpservices.settings');	
    $dp_request_url = $config->get('dp_request_url');    
    $fileID = [];    
    foreach($values['value'] as $file_key => $value) {
      $path = $value['path'];
      $image_path = $value['image_path'];
      $dir_name = pathinfo($path);     
      $dir_name = $dir_name['dirname'];
      $res = strpos($dir_name , '//');
      if($res === false) {
        $dir_name = $dir_name . '//';
      }      
      if(file_prepare_directory($dir_name, FILE_CREATE_DIRECTORY)) {		  
        $file = file_get_contents($image_path);        
        $file = file_save_data($file, $path , FILE_EXISTS_RENAME);        
        $fileID[] = $file->id();
      }	
    }
	  return $fileID;
  }

  /**
   * Generate Storage Entity Array for Paragraph.
   */
  public static function getEntityArrayByParagraph($values, $key, $arr_paragraph, $node) {
   
    $paragraph = [];    
    foreach($values[$key] as $index => $value) {
      $type = $value['type'];
      $bundleFields = FieldDetails::getFieldList('paragraph', $type);
      $arr_paragraph['type'] = $type;
      foreach($bundleFields['paragraph'] as $field_name => $field) {
        $data = $value[$field_name];
        if($field['datatype'] == 'paragraph') {         
          $data = [];
          $data[$field_name] = $value[$field_name];
          $arr_paragraph[$field_name] = self::getEntityArrayByChildParagraph($data, $field_name);
        }
        else if($field['datatype'] == 'file') {
          $arr_data['value'] = $data;
          $fileID = self::getEntityArrayByFile($arr_data);
          $arr_paragraph[$field_name] = $fileID;
        }
        else {
          $arr_paragraph[$field_name] = $data;
        }
      }
      $paragraph = \Drupal\paragraphs\Entity\Paragraph::create($arr_paragraph);
      $paragraph->isNew();      
      $field = $node->get($key);
      $field->appendItem($paragraph);            
    } 
	  return $node;
  }

  /**
   * Generate Storage Entity Array for Paragraph.
   */
  public static function getEntityArrayByChildParagraph($values, $key) {
   
    $paragraph = [];    
    foreach($values[$key] as $index => $value) {
      $type = $value['type'];
      $bundleFields = FieldDetails::getFieldList('paragraph', $type);
      $arr_paragraph['type'] = $type;
      foreach($bundleFields['paragraph'] as $field_name => $field) {
        $data = $value[$field_name];
        if($field['datatype'] == 'paragraph') {
          $data = [];
          $data[$field_name] = $value[$field_name];
          $arr_paragraph[$field_name] = self::getEntityArrayByChildParagraph($data, $field_name);
        }
        else if($field['datatype'] == 'file') {
          $arr_data['value'] = $data;
          $fileID = self::getEntityArrayByFile($arr_data);
          $arr_paragraph[$field_name] = $fileID;
          
        }
        else {
          $arr_paragraph[$field_name] = $data;
        }
      }
      $paragraph_criteria[] = Paragraph::create(
        $arr_paragraph      
      );    
    } 
   
	  return $paragraph_criteria;
  }

  /**
   * Delete Paragraph if exists.
   */
  public static function deleteParagraphIfExists($paragraph) {  
    foreach ( $paragraph as $element ) {
      if(isset($element['target_id'])) {
        $paragraph_obj = Paragraph::load($element['target_id']);
        if(isset($paragraph_obj)) {
          $paragraph_obj->delete();
        } 
      }
    }
  }
  
}
