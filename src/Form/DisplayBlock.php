<?php

/**
 * @file
 * Contains \Drupal\dpservices\Form\DisplayBlock.
 */
namespace Drupal\dpservices\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dpservices\DpservicesUtilities;
use Drupal\dpservices\DpservicesConfigDetails;
use Drupal\dpservices\EntityTranslationDetails;
use Drupal\block_content\Entity\BlockContent;

class DisplayBlock extends FormBase {  

  private $dp_username;
  private $dp_password;

  public function __construct() {
    $this->setDeploySettings();   
  }

  public function getFormId() {
    return 'deploy_block_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {   
    if(db_table_exists('block_content__' . DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY)) {
      $results = [];
      $header = [
        [ 'data' => $this->t('Entity ID'), 'field' => 'b.id'],
        [ 'data' => $this->t('Title')],
        [ 'data' => $this->t('type')],
        [ 'data' => $this->t('langcode')],
        [ 'data' => $this->t('changed'), 'field' => 'bfd.changed', 'sort' => 'desc']           
      ];
	    $query = \Drupal::database()->select('block_content', 'b');
      $query->join('block_content__' . DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY , 'd', 'd.entity_id = b.id');
	    $query->join('block_content_field_data' , 'bfd', 'bfd.id = b.id');
      $query->fields('b', ['id']);
	    $query->fields('bfd', ['changed']); 
      $query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);     
	    $query->condition('d.' . DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY . '_value', 1);
      //For the pagination we need to extend the pagerselectextender and
      //limit in the query
      $config = $this->getDeploySettings();
      $pager_count = $config->get('pager');     
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit($pager_count);      
      $results_obj = $pager->execute()->fetchAll(); 
      foreach($results_obj as $obj) {
        $results[$obj->id] = $obj->id;
      }	    
      
	    $output = array();
      $lang_codes = EntityTranslationDetails::getSiteLanguagesCode();
      foreach ($results as $bid) {       
        if(isset($bid)) {
          foreach ($lang_codes as $langcode) {
            $block = BlockContent::load($bid);
            $lang_status = EntityTranslationDetails::checkIfTranslationExists($block, $langcode);            
            if($lang_status) {
              $block = EntityTranslationDetails::getEntityTranslation($block, $langcode);
              $deploy_status = $block->get(DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY)->getvalue();
              if(isset($deploy_status) && $deploy_status[0]['value']) {
                $title = $block->get('info')->getvalue()[0]['value'];
                $type = $block->get('type')->getvalue()[0]['target_id'];                              
                $updated = $block->get('changed')->getvalue()[0]['value'];
                $query = \Drupal::database()->select('dpservices' , 'n');
                $query->fields('n' , array('entity_id'));
                $query->condition('status', 0, "=");
                $query->condition('type', 'block_content', "=");
                $query->condition('entity_id', $bid, "=");
                $query->condition('langcode', $langcode, "=");
                $results = $query->execute()->fetchAll();
                if(count($results) == 0) {
                  $output[$bid . '_' . $langcode][] = $bid;
                  $output[$bid . '_' . $langcode][] = $title;
                  $output[$bid . '_' . $langcode][] = $type;
                  $output[$bid . '_' . $langcode][] = $langcode;
                  $output[$bid . '_' . $langcode][] = date("Y-m-d H:i:s" , $updated);
                }
              }             
            }
          }          		
	      }
      }	
      $form['table'] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $output,
        '#empty' => $this->t('No blocks available for deployment.'),
      ];
      if(count($output) != 0) { 
        if(!isset($this->dp_username) || !isset($this->dp_password)) {
	        drupal_set_message($this->t('Set username and password in deploy settings page for authentication.'), 'warning');	             
	      } 
        $form['datetime'] = [
          '#title' => 'Chose date to deploy (eg : m/d/Y h:i:sa) :',
          '#type' => 'datetime',
          '#size' => 20,
	        '#date_time_element' => 'text',
	        '#default_value' => '',
	        '#required' => true,
        ];
	      $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Save'),
          '#button_type' => 'primary',
        );
	      $form['pager'] = array(
          '#type' => 'pager'
        );
	    }
      return $form;
    }
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if(!isset($this->dp_username) || !isset($this->dp_password)) {
      drupal_set_message($this->t('For authentication, update username and password in deploy settings page.'), 'error');	             
    }
    else {
      $date = $form_state->getValue('datetime')->format('d-m-Y h:i:sa');
      $date = strtotime($date);
      $arr_default_bid = $form_state->getValue('table');
      if(count($arr_default_bid) != 0) {
        foreach($arr_default_bid as $value) {	
          $arr_value = explode('_', $value);  
          if(isset($value) && !empty($value)) {
            $default_langcode = $this->getEntityCreatedDate($arr_value[0], $arr_value[1]);
            $result = \Drupal::database()->insert('dpservices')
            ->fields(['entity_id' => $arr_value[0] , 'type' => 'block_content' , 'deploy_date' => $date, 'langcode' => $arr_value[1], 'default_langcode' => $default_langcode])->execute();
          }	  
        }
      }
      drupal_set_message($this->t('Deployment for Block submitted successfully.'));
    }
    		
  }
  
  public function setDeploySettings() {
    $config = DpservicesConfigDetails::getDeployConfig();
    $this->dp_username = $config->get('username');
	  $this->dp_password = $config->get('pass');
  }

  public function getEntityCreatedDate($bid, $langcode) {
    $block = BlockContent::load($bid);    
    $block = EntityTranslationDetails::getEntityTranslation($block, $langcode);    
    return $block->get('default_langcode')->getvalue()[0]['value'];
  }

  public function getDeploySettings() {
    $config = DpservicesConfigDetails::getDeployConfig();
    return $config;
  }
} 