<?php

/**
 * @file
 * Contains \Drupal\dpservices\Form\Deploysettings.
 */
namespace Drupal\dpservices\Form;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dpservices\DpservicesUtilities;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

class DpservicesSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'dpservices.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dpservices_admin_settings';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }	
	  $blockTypes = \Drupal::service('entity.manager')->getStorage('block_content_type')->loadMultiple();
    $blockTypesList = [];
    foreach ($blockTypes as $contentType) {
      $blockTypesList[$contentType->id()] = $contentType->label();
    }	
    
    $form['request_url'] = [
      '#type' => 'textfield',
      '#title' => 'Request Url',
	    '#description' => 'Request Url should be included with http:// or https://',
      '#default_value' => $config->get('request_url'),
      '#required' => true,      
    ];	
	  $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#size' => 25,
	    '#description' => 'Enter username for authentication',
	    '#default_value' => $config->get('username'),
      '#required' => true,	  
    ];
	  if(empty($config->get('pass'))) {
	    $form['pass'] = [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#size' => 25,
        '#description' => 'Enter password for authentication',
	      '#default_value' => $config->get('pass'),	  
        '#required' => true,  
      ];	
	  }
	  else {		
      $form['pass'] = [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#size' => 25,
	      '#description' => 'Enter password for authentication',
	      '#default_value' => $config->get('pass'),
      ];
	  }	
	  $form['content_type'] = [
      '#type' => 'checkboxes',
      '#options' => $contentTypesList,
      '#title' => $this->t('Content Type'),
      '#default_value' => !empty($config->get('content_type'))? $config->get('content_type') : [],
      '#description' => $this->t('Please choose the content type to deploy the block.'),
    ];
	  $form['block_type'] = [
      '#type' => 'checkboxes',
      '#options' => $blockTypesList,
      '#title' => $this->t('Block Type'),  
	    '#default_value' =>!empty($config->get('block_type'))? $config->get('block_type') : [],
      '#description' => $this->t('Please choose the block type to deploy the block.'),
    ];
    $form['pager'] = [
      '#type' => 'select',
      '#title' => $this->t('Pagination Records'),
      '#options' => [10 => 10, 25 => 25, 50 => 50, 100 => 100 ],
      '#default_value' => $config->get('pager'),
      '#description' => $this->t("Number of records per page."),
      '#required' => TRUE,
      '#weight' => 20,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::SETTINGS);
    $values = $form_state->getValues();   
    $config->set('request_url', $values['request_url']);
    $config->set('username', $values['username']);
    if(!empty($values['pass'])) {
      $config->set('pass', $values['pass']);
    }   
    $config->set('content_type', $values['content_type']);
    $config->set('block_type', $values['block_type']);
    $config->set('pager', $values['pager']);
    $values['content_type'] = array_filter($values['content_type']);
    $values['block_type'] = array_filter($values['block_type']);
    if(count($values['content_type']) > 0) {
      $field_config = $this->checkNodeFieldName(DpservicesUtilities::DP_FIELD_NODE_DEPLOY , 'node');	  
	    if(!isset($field_config)) {
	      $this->createNodeFieldName(DpservicesUtilities::DP_FIELD_NODE_DEPLOY , 'node');
	    }
      foreach($values['content_type'] as $key => $value) {
	      $node_type_field = $this->checkNodeTypeFieldName(DpservicesUtilities::DP_FIELD_NODE_DEPLOY , 'node' , $value);		  
        if(!isset($node_type_field)) {
          $this->createNodeTypeFieldName(DpservicesUtilities::DP_FIELD_NODE_DEPLOY , 'node' , $value);                       
        }        	  
	    }
    }
    if(count($values['block_type']) > 0) {
      $field_config = $this->checkNodeFieldName(DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY , 'block_content');	  
	    if(!isset($field_config)) {
	      $this->createNodeFieldName(DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY , 'block_content');
	    }
      foreach($values['block_type'] as $key => $value) {	  
	      $node_type_field = $this->checkNodeTypeFieldName(DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY , 'block_content' , $value);		  
		    if(!isset($node_type_field)) {
		      $this->createNodeTypeFieldName(DpservicesUtilities::DP_FIELD_BLOCK_DEPLOY , 'block_content' , $value);
		    }	  
	    }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  public function checkNodeFieldName($field_name , $entity_type) {
    $field_config = FieldStorageConfig::loadByName($entity_type , $field_name);	
	  return $field_config;
  }

  public function checkNodeTypeFieldName($field_name , $entity_type , $content_type) {
    $field_config = FieldConfig::loadByName($entity_type , $content_type , $field_name);
	  return $field_config;
  }

  public function createNodeFieldName($field_name , $entity_type) {
    FieldStorageConfig::create(array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'type' => 'boolean',
      'cardinality' => 1,
	    'settings' => array(
      'allowed_values' => array(0 => 0, 1 => 1), //add the default values for the checkbox
      ),
    ))->save();
  }

  public function createNodeTypeFieldName($field_name , $entity_type , $content_type) {
    FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $content_type,
      'label' => $this->t('Deploy Status'),
    ])->save();
	  entity_get_form_display($entity_type, $content_type, 'default')
       ->setComponent($field_name , array(
         'type' => 'boolean_checkbox',
    ))->save();
  }
}
