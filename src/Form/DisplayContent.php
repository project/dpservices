<?php

/**
 * @file
 * Contains \Drupal\dpservices\Form\DisplayContent.
 */
namespace Drupal\dpservices\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dpservices\DpservicesUtilities;
use Drupal\dpservices\DpservicesConfigDetails;
use Drupal\dpservices\EntityTranslationDetails;
use Drupal\node\Entity\Node;


class DisplayContent extends FormBase {
  
  private $dp_username;
  private $dp_password;

  public function __construct() {
    $this->setDeploySettings();   
  }

  public function getFormId() {
    return 'deploy_node_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    if(db_table_exists('node__' . DpservicesUtilities::DP_FIELD_NODE_DEPLOY)) {
      $results = []; 
      $header = [
        [ 'data' => $this->t('Entity ID'), 'field' => 'n.nid'],
        [ 'data' => $this->t('Title')],
        [ 'data' => $this->t('type')],
        [ 'data' => $this->t('langcode')],
        [ 'data' => $this->t('changed'), 'field' => 'nfd.changed', 'sort' => 'desc']           
      ]; 
      $query = \Drupal::database()->select('node', 'n');
      $query->distinct();
      $query->join('node__' . DpservicesUtilities::DP_FIELD_NODE_DEPLOY , 'd', 'd.entity_id = n.nid');
	    $query->join('node_field_data' , 'nfd', 'nfd.nid = n.nid');
      $query->fields('n', ['nid']);
	    $query->fields('nfd', ['changed']);      
      $query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);	
	    $query->condition('d.' . DpservicesUtilities::DP_FIELD_NODE_DEPLOY . '_value', 1);
      //For the pagination we need to extend the pagerselectextender and
      //limit in the query
      $config = $this->getDeploySettings();
      $pager_count = $config->get('pager');     
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit($pager_count);
      $results_obj = $pager->execute()->fetchAll(); 
      foreach($results_obj as $obj) {
        $results[$obj->nid] = $obj->nid;
      }     
		 
	    $output = array();
      $lang_codes = EntityTranslationDetails::getSiteLanguagesCode();
    
	    foreach ($results as $nid) {
	      if(isset($nid)) {
          foreach ($lang_codes as $langcode) {
            $node = Node::load($nid);
            $lang_status = EntityTranslationDetails::checkIfTranslationExists($node, $langcode);
            if($lang_status) {
              $node = EntityTranslationDetails::getEntityTranslation($node, $langcode);
              $deploy_status = $node->get(DpservicesUtilities::DP_FIELD_NODE_DEPLOY)->getvalue();
              if(isset($deploy_status) && $deploy_status[0]['value']) {
                $title = $node->get('title')->getvalue()[0]['value'];
                $type = $node->get('type')->getvalue()[0]['target_id'];                              
                $updated = $node->get('changed')->getvalue()[0]['value'];
                $query = \Drupal::database()->select('dpservices' , 'n');
                $query->fields('n' , array('entity_id'));
                $query->condition('status', 0, "=");
                $query->condition('type', 'node', "=");
                $query->condition('entity_id', $nid, "=");
                $query->condition('langcode', $langcode, "=");
                $results = $query->execute()->fetchAll();
                if(count($results) == 0) {
                  $output[$nid . '_' . $langcode][] = $nid;
                  $output[$nid . '_' . $langcode][] = $title;
                  $output[$nid . '_' . $langcode][] = $type;
                  $output[$nid . '_' . $langcode][] = $langcode;
                  $output[$nid . '_' . $langcode][] = date("Y-m-d H:i:s" , $updated);
                }
              }             
            }
          }	    		
	      }
      }	
      $form['table'] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $output,
        '#empty' => $this->t('No contents available for deployment.'),
        '#theme' => 'table'
      ];
      if(count($output) != 0) {        
	      if(!isset($this->dp_username) || !isset($this->dp_password)) {
	        drupal_set_message($this->t('Set username and password in deploy settings page for authentication.'), 'warning');	             
	      }
        $form['datetime'] = [
          '#title' => 'Chose date to deploy (eg : m/d/Y h:i:sa) :',
          '#type' => 'datetime',
          '#size' => 20,
	        '#date_time_element' => 'text',
	        '#default_value' => '',
	        '#required' => true,
        ];
	      $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Save'),
          '#button_type' => 'primary',
        );
	      $form['pager'] = array(
          '#type' => 'pager'
        );
	    }
      return $form;
    }
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if(!isset($this->dp_username) || !isset($this->dp_password)) {
      drupal_set_message($this->t('For authentication, update username and password in deploy settings page.'), 'error');	             
    }
    else {     
      $date = $form_state->getValue('datetime')->format('d-m-Y h:i:sa');
	    $date = strtotime($date);
	    $arr_default_nid = $form_state->getValue('table');     
	    if(count($arr_default_nid) != 0) {
        foreach($arr_default_nid as $value) {
          $arr_value = explode('_', $value);
	        if(isset($value) && !empty($value)) {
            $default_langcode = $this->getEntityCreatedDate($arr_value[0], $arr_value[1]);
            $result = \Drupal::database()->insert('dpservices')
	          ->fields(['entity_id' => $arr_value[0], 'type' => 'node' , 'deploy_date' => $date, 'langcode' => $arr_value[1], 'default_langcode' => $default_langcode])->execute();
	        }	  
	      }
	    }
      drupal_set_message($this->t('Deployment for node submitted successfully.'));
    }		
  }
  public function setDeploySettings() {
    $config = DpservicesConfigDetails::getDeployConfig();
    $this->dp_username = $config->get('username');
	  $this->dp_password = $config->get('pass');
  }

  public function getEntityCreatedDate($nid, $langcode) {
    $node = Node::load($nid);
    $node = EntityTranslationDetails::getEntityTranslation($node, $langcode);
    return $node->get('default_langcode')->getvalue()[0]['value'];
  }

  public function getDeploySettings() {
    $config = DpservicesConfigDetails::getDeployConfig();
    return $config;
  }
}