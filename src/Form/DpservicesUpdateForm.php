<?php

/**
 * @file
 * Contains \Drupal\dpservices\Form\Deploysettings.
 */
namespace Drupal\dpservices\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Datetime\DrupalDateTime;


class DpservicesUpdateForm extends FormBase {
 
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dpservices_update_form';
  }
  /**
   * {@inheritdoc}
   */
  public $id;
  public function buildForm(array $form, FormStateInterface $form_state , $id = NULL) {
	$this->id = $id;
	$query = \Drupal::database()->select('dpservices' , 'n');
    $query->fields('n' , array('id' , 'entity_id' , 'type' , 'deploy_date' , 'status'));
    $query->condition('id' , $this->id , '=');    
    $results = $query->execute()->fetchAll();
    $form['datetime'] = [
      '#title' => $this->t('Update date to deploy :'),
      '#type' => 'datetime',
      '#size' => 20,
	    '#date_time_element' => 'text',
	    '#default_value' => DrupalDateTime::createFromTimestamp($results[0]->deploy_date),
	    '#required' => true,      
    ];
	$form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );	
	return $form;   
  }
  /**
   * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	$date = $form_state->getValue('datetime')->format('d-m-Y h:i:sa');
	$date = strtotime($date);
	$query = \Drupal::database()->update('dpservices');
    $query->fields(['deploy_date' => $date ]);
    $query->condition('id', $this->id);
	$query->execute();
	drupal_set_message($this->t('Succesfully updated.'));
    $form_state->setRedirect('dpservices.deploy_status');    
  }
  
}
