<?php

namespace Drupal\dpservices\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;


/**
 * Class DpservicesDeleteForm.  
 */
class DpservicesDeleteForm extends ConfirmFormBase {
	
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dpservices_delete_form';
  }
  
  public $id;
  
  public function getQuestion() { 
    return $this->t('Do you want to delete the selected schedule?');
  }
  
  public function getCancelUrl() {
   return new Url('dpservices.deploy_status');
  }
  
  public function getDescription() {
   return $this->t('Selected schedule will be deleted.');
  }
  
  /**
   * {@inheritdoc}
   */
   
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
   
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {	  
    $query = \Drupal::database();
    $query->delete('dpservices')
      ->condition('id' , $this->id)
      ->execute();
    drupal_set_message($this->t('Selected schedule deleted succesfully.'));
    $form_state->setRedirect('dpservices.deploy_status');
  }
}
