<?php

/**
 * @file
 * Contains \Drupal\dpservices\Controller\DeployStatusController.
 */

namespace Drupal\dpservices\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\block_content\Entity\BlockContent;
use Drupal\dpservices\EntityTranslationDetails;
use Drupal\dpservices\DpservicesConfigDetails;



/**
 * Controller routines for deploy status.
 */
class DeployStatusController extends ControllerBase {

  public function deploy_status() {
	$header = [
	  [ 'data' => $this->t('Entity ID'), 'field' => 'entity_id'],
	  [ 'data' => $this->t('Title')],
	  [ 'data' => $this->t('Entity Type')],
	  [ 'data' => $this->t('Bundle'), 'field' => 'type'],
	  [ 'data' => $this->t('langcode'), 'field' => 'langcode'],	  
	  [ 'data' => $this->t('Status'), 'field' => 'status'],
	  [ 'data' => $this->t('Deploy Date'), 'field' => 'deploy_date', 'sort' => 'desc'],
	  [ 'data' => $this->t('Update')],
	  [ 'data' => $this->t('Delete')],    
	];  
    $query = \Drupal::database()->select('dpservices' , 'n');
    $query->fields('n' , array('id' , 'entity_id' , 'type' , 'deploy_date' , 'status', 'langcode'));
	$query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $config = $this->getDeploySettings();
    $pager_count = $config->get('pager');     
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit($pager_count); 
    $results = $pager->execute()->fetchAll();	
    
	$output = array();	
    foreach ($results as $value) {
	  if($value->type == 'node') {
		$node = Node::load($value->entity_id);
		$node = EntityTranslationDetails::getEntityTranslation($node, $value->langcode);	
		$title = $node->get('title')->getvalue()[0]['value'];	   
		$type = $node->get('type')->getvalue()[0]['target_id'];   
		$langcode = $value->langcode;
	  }
	  else if($value->type == 'block_content') {
		$block = BlockContent::load($value->entity_id);
        $block = EntityTranslationDetails::getEntityTranslation($block, $value->langcode);	
		$title = $block->get('info')->getvalue()[0]['value'];		
		$type = $block->get('type')->getvalue()[0]['target_id'];		
		$langcode = $value->langcode;	
	  }
	  $status = $this->t('Pending');
	  if($value->status == 1) {
	    $status = $this->t('Completed');
	  }
	  $delete = '-';
	  $update = '-';
	  if($value->status == 0) {
	    $delete = Url::fromUserInput('/admin/dpservices/deploy/delete/' . $value->id);
	    $update = Url::fromUserInput('/admin/dpservices/deploy/update/' . $value->id);
		$update = \Drupal::l('Update', $update);
		$delete = \Drupal::l('Delete', $delete);		
	  }
      $rows[] = array('data' => array(
		'entity_id' => $value->entity_id,
        'title' => $title,
		'Entity Type' => $value->type,
        'Bundle' => $type,
		'langcode' => $langcode,		
		'status' => $status,
		'deploy_date' =>  date("Y-m-d H:i:s" , $value->deploy_date),
		$update,
		$delete,
      ));	  
	  
    }
	 $build = array(
      '#markup' => '<h5>' . $this->t('Deployment status') . ':</h5>',
    );
	// Generate the table.
    $build['config_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
 
    // Finally add the pager.
    $build['pager'] = array(
      '#type' => 'pager'
    );
 
    return $build;
  }

  public function getDeploySettings() {
    $config = DpservicesConfigDetails::getDeployConfig();
    return $config;
  }
   
}
