<?php

/**
 * @file
 * Contains \Drupal\dpservices\Controller\ResponseAPIController.
 */

namespace Drupal\dpservices\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\block_content\Entity\BlockContent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\dpservices\EntityDetails;
use Drupal\dpservices\DpservicesGenerateStorageEntityArray;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\dpservices\EntityTranslationDetails;

/**
 * Controller routines for deploy entity.
 */
class ResponseAPIController extends ControllerBase {

  /**
   * Callback for 'dpservices/deploy/entity.json' API method.
   */
  public $arr_fcollection;
  public $arr_nid , $node_type , $bid , $arr_bid , $block_type;
   
  public function deployRequest( Request $request ) {   
	// This condition checks the `Content-type` and makes sure to 
    // decode JSON string from the request body into array.
    if ( 0 === strpos( $request->headers->get( 'Content-Type' ), 'application/json' ) ) {
      $data = json_decode( $request->getContent(), TRUE );
      $request->request->replace( is_array( $data ) ? $data : [] );
    }
	if(isset($data['node']) && count($data['node']) > 0) {	
      foreach($data['node'] as $values) {
		$id = $values['nid']['value'][0]['value'];
		$arr_nodeID = EntityDetails::checkNodeExists($id);
		$langcode = $values['langcode']['value'];
	    if(count($arr_nodeID) === 0) {
		  $node = Node::create([
		    'type' => $values['type']['value'][0]['target_id'],
			'langcode' => $langcode,
			'nid' => $id        
		  ]);
		  foreach($values['entity'] as $key => $value) {			
			if(isset($value['type'])) {
			  if($value['type'] == 'paragraph') {	
				$arr_paragraph = [];
				$node = DpservicesGenerateStorageEntityArray::getEntityArrayByParagraph($value['value'], $key, $arr_paragraph, $node);
			  }
			  else if($value['type'] == 'file') {				
				$fileID = DpservicesGenerateStorageEntityArray::getEntityArrayByFile($value);	
				$node->set($key, $fileID);
			  }
		    }
			else {
				$node->set($key, $value['value']);			  
			}
		  }
		  $node->save();
		  $this->arr_nid[] = [
		    'id' => $id,
			'langcode' => $langcode
		  ];		
		}
		else {
		  $arr_nodeID = array_values($arr_nodeID);
		  $nid = $arr_nodeID[0];
		  $node = Node::load($nid);	
		  $translated_status = EntityTranslationDetails::checkIfTranslationExists($node, $langcode);	
		  if($translated_status) {
		    $node = EntityTranslationDetails::getEntityTranslation($node, $langcode);
		  }
		  else {
		    $node = EntityTranslationDetails::addEntityTranslation($node, $langcode);
		  }
		  foreach($values['entity'] as $key => $value) {
		    if(isset($value['type'])) {
			  if($value['type'] == 'paragraph') {	
			  $arr_paragraph = [];
			  $paragraph = $node->$key->getValue();			
			    DpservicesGenerateStorageEntityArray::deleteParagraphIfExists($paragraph);			
			    $node = DpservicesGenerateStorageEntityArray::getEntityArrayByParagraph($value['value'], $key, $arr_paragraph, $node);
			  }
			  else if($value['type'] == 'file') {				
			    $fileID = DpservicesGenerateStorageEntityArray::getEntityArrayByFile($value);	
			    $node->$key = $fileID;
			  }
		    }			
		    else {
		      $node->$key = $value['value'];	  
		    }	
		  }
		  $node->save();
		  $this->arr_nid[] = [
		    'id' => $id,
			'langcode' => $langcode
		  ];
	    }     
	  }
	}
	if(isset($data['block']) && count($data['block']) > 0) {	
	  foreach($data['block'] as $values) {
		$id = $values['bid']['value'][0]['value'];
		$arr_blockID = EntityDetails::checkBlockExists($id);
		$langcode = $values['langcode']['value'];
		if(count($arr_blockID) === 0) {
		  $block = BlockContent::create([
			'type' => $values['type']['value'][0]['target_id'],
			'langcode' => $langcode,
			'id' => $id  
		  ]);			  
		  foreach($values['entity'] as $key => $value) {			
			if(isset($value['type'])) {
			  if($value['type'] == 'paragraph') {	
				$arr_paragraph = [];
				$block = DpservicesGenerateStorageEntityArray::getEntityArrayByParagraph($value['value'], $key, $arr_paragraph, $block);
			  }
			  else if($value['type'] == 'file') {				
				$fileID = DpservicesGenerateStorageEntityArray::getEntityArrayByFile($value);	
				$block->set($key, $fileID);
			  }
			}
			else {
			  $block->set($key, $value['value']);			  
			}
		  }
		  $block->save();
		  $this->arr_bid[] = [
		    'id' => $id,
			'langcode' => $langcode
		  ];			
		}
		else {
		  $arr_blockID = array_values($arr_blockID);
		  $bid = $arr_blockID[0];
		  $block = BlockContent::load($bid);		  
		  $translated_status = EntityTranslationDetails::checkIfTranslationExists($block, $langcode);	
		  if($translated_status) {
			$block = EntityTranslationDetails::getEntityTranslation($block, $langcode);
		  }
		  else {
			$block = EntityTranslationDetails::addEntityTranslation($block, $langcode);
		  }
		  foreach($values['entity'] as $key => $value) {
			if(isset($value['type'])) {
			  if($value['type'] == 'paragraph') {	
				$arr_paragraph = [];
				$paragraph = $block->$key->getValue();				
				DpservicesGenerateStorageEntityArray::deleteParagraphIfExists($paragraph);				
				$block = DpservicesGenerateStorageEntityArray::getEntityArrayByParagraph($value['value'], $key, $arr_paragraph, $block);
			  }
			  else if($value['type'] == 'file') {				
				$fileID = DpservicesGenerateStorageEntityArray::getEntityArrayByFile($value);	
				$block->$key = $fileID;
			  }
			}			
			else {
			  $block->$key = $value['value'];	  
			}	
		  }
		  $block->save();	
		  $this->arr_bid[] = [
		    'id' => $id,
			'langcode' => $langcode
		  ];
		}     
	  }
	}    
    $res_node['node'] = $this->arr_nid;
    $res_node['block'] = $this->arr_bid;
	$response['data'] = $res_node;
    $response['method'] = 'POST';
    return new JsonResponse( $response );	
  }
}
