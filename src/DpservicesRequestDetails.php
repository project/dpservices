<?php

namespace Drupal\dpservices;

use Drupal\dpservices\DpservicesUtilities;
use Drupal\Component\Serialization\Json;
use Drupal\dpservices\DpservicesConfigDetails;

/**
 * Defines Request functionalities. 
 */
class DpservicesRequestDetails {
  
  /**
   * Define function to call the webservice url.
   */
  public static function getServiceOptions($arr_node) {
    $config = DpservicesConfigDetails::getDeployConfig();	
    $dp_username = $config->get('username');
	  $dp_password = $config->get('pass');
    $serialized_entity = Json::encode($arr_node);
    $options = [
      'auth' => [$dp_username , $dp_password],
      'headers' => [
      'Content-Type' => 'application/json',
      ],
      'body' => $serialized_entity
    ];
    return $options;
  }

  /**
   * Define function to call the webservice url.
   */
  public static function getDeployRequest($options) {
    $config = \Drupal::config('dpservices.settings');
    $client = \Drupal::httpClient();
    $url = $config->get('request_url') . DpservicesUtilities::DP_REQUEST_URL;
    $method = 'POST';
    try {
      $response = $client->request($method, $url, $options);
      $code = $response->getStatusCode();
      $body = '';
      if ($code == 200) {
        $body = $response->getBody()->getContents();
        $body = Json::decode($body);
        $message = 'Deployment for entities submitted successfully.';
        \Drupal::logger('dpservices')->notice($message);
      }
    }
    catch (RequestException $e) {
      $message = 'There are some issues on deploying entities.';
      \Drupal::logger('dpservices')->notice($message);
    }
    return $body;
  } 
  
}
