<?php

namespace Drupal\dpservices;

use Drupal\file\Entity\File;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\dpservices\EntityTranslationDetails;

/**
 * Generate Entity Array.
 */
class DpservicesGenerateEntityArray {

  /**
   * Generate Entity Array for File Type.
   */
  public static function getEntityArrayByFileType($values , $key , $id, $node, $datatype) {
    foreach($values as $arr_key=> $value) {	  
	    $fid = $value['target_id'];
	    $file = File::load($fid);
	    $path = $file->getFileUri();    
      $image_path = file_create_url($path);	
	    $node[$id]['entity'][$key]['value'][$arr_key]['path'] = $path;
	    $node[$id]['entity'][$key]['value'][$arr_key]['image_path'] = $image_path;   
      $node[$id]['entity'][$key]['type'] = $datatype;
	  }
	  return $node;
  }

  /**
   * Generate Entity Paragraph Array for File Type.
   */
  public static function getEntityParagraphArrayByFileType($values) {
    $fileID = [];
    foreach($values as $arr_key=> $value) {	  
	    $fid = $value['target_id'];
	    $file = File::load($fid);
	    $path = $file->getFileUri();    
      $image_path = file_create_url($path);	
	    $fileID[$arr_key]['path'] = $path;
	    $fileID[$arr_key]['image_path'] = $image_path;
	  }
	  return $fileID;
  }
  
  /**
   * Generate Entity Array for Url Alias Type.
   */
  public static function getEntityArrayByPath($value , $key , $id, $node, $datatype) {
    if(isset($value[0]['alias'])) {
	    $node[$id]['entity'][$key]['value'] = $value;
      $node[$id]['entity'][$key]['type'] = $datatype;
	  }
	  return $node;
  }

  /**
   * Generate Entity Array for datetime field.
   */
  public static function getEntityArrayByDateTime($value , $key , $id, $node, $datatype) {
	if(isset($value[0])) {
	  $timestamp = strtotime($value[0]['value']);
	  $node[$id]['entity'][$key]['value'] = date('Y-m-d', $timestamp);	 
    $node[$id]['entity'][$key]['type'] = $datatype; 
	}    
    return $node;	
  }

  /**
   * Generate Entity Array for daterange field.
   */
  public static function getEntityArrayByDateRange($value , $key , $id, $node, $datatype) {
	  $arr_date = [];
    if(isset($value[0])) {      
      $start = strtotime($value[0]['value']);
      $end = strtotime($value[0]['end_value']);      
      $arr_date['value'] = date('Y-m-d', $start);
      $arr_date['end_value'] = date('Y-m-d', $end);
	    $node[$id]['entity'][$key]['value'] = $arr_date; 
      $node[$id]['entity'][$key]['type'] = $datatype;
    }     
    return $node;	
  }

  /**
   * Generate Entity Array for daterange field.
   */
  public static function getEntityArrayByParagraph($paragraphs, $key, $node,  $arr_paragraph, $langcode) {
	  $paragraph = [];   
   
    foreach($paragraphs as $index => $paragraph) {
      //$paragraph = Paragraph::load($value['target_id']);  
      $lang_status = EntityTranslationDetails::checkIfTranslationExists($paragraph, $langcode);
      if($lang_status) {
        if(isset($paragraph)) {
          $paragraph = EntityTranslationDetails::getEntityTranslation($paragraph, $langcode);
        }
      }      
      $type = $paragraph->get('type')->getvalue()[0]['target_id'];
      $bundleFields = FieldDetails::getFieldList('paragraph', $type);
      $arr_paragraph[$key][$index]['type'] = $type;     
      foreach($bundleFields['paragraph'] as $field_name => $field) {
        $data = $paragraph->get($field_name)->getvalue();
        if($field['datatype'] == 'paragraph') {  
          $data = $paragraph->get($field_name)->referencedEntities();    
          $arr_paragraph[$key][$index] = self::getEntityArrayByParagraph($data, $field_name, $node, $arr_paragraph[$key][$index], $langcode);         
        }
        else if($field['datatype'] == 'file') {
          $fileID = self::getEntityParagraphArrayByFileType($data);
          $arr_paragraph[$key][$index][$field_name] = $fileID;
        }
        else {
          $arr_paragraph[$key][$index][$field_name] = $data;  
          if(!isset($arr_paragraph[$key][$index]['status'])) {
            $arr_paragraph[$key][$index]['status'][0]['value'] = 1;
          } 
          if(!isset($arr_paragraph[$key][$index]['created'])) {
            $arr_paragraph[$key][$index]['created'][0]['value'] = time();
          }         
        }
      }     
      
    }
    return $arr_paragraph;	
  }
  
}
