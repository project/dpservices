<?php

namespace Drupal\dpservices;


/**
 * Defines Config Details. 
 */
class DpservicesConfigDetails {
  
  /**
   * Define function to get Config Details.
   */
  public static function getDeployConfig() {
    return \Drupal::config('dpservices.settings');
  }  
  
}
