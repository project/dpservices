CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

It provides a user interface to deploy the entities(content and block) from one server to another server 
on schedule basis.

Example:  Staging to Production.

It will support to push the content and block from staging to production as well as production to staging.

This module will support for the below fields,

- All core fields(eg: Text, Date, File, Image, Entity reference)
- Paragraph

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install this module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.

CONFIGURATION
-------------

Usage :

(We need to install this module on both server eg: Staging and Production.)

After install this module, please follow the below steps :

1) In the admin top menu, click Deploy Services->Settings link under the Configuration->Development menu.
2) Enter the request url to push the content and block.
3) Before updating the username and password, We need to add new user with specific role(eg: Deploy Services) or use existing user and grant the
   "Access Rest Dpservices" permission for the role in the particular user in the requested server(production).
4) Enter the username and password for authentication.
5) Select the content type and block type that you want to push to another server.
6) For pushing the content, edit the content and check the "Deploy Status" field(field_node_deploy).
7) For pushing the block, edit the block and check the "Deploy Status" field(field_block_deploy).
8) The above created content/block will be available for deployment in the deploy content/block list page.
   Select the content and schedule the time to deploy whenever cron process run.

Note : All the fields name should be same in the both sites.

MAINTAINERS
-----------

Current maintainers:
  Elavarasan R - https://www.drupal.org/user/1902634
